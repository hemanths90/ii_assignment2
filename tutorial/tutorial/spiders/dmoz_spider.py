from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.spiders import CrawlSpider,Rule
from scrapy.linkextractors import LinkExtractor
from tutorial.items import LaptopItem
import pdb
import sys

nPages=0;

class DmozSpider(CrawlSpider):
    name = "laptopsCrawl"
    allowed_domains = ["amazon.com"]
    start_urls = [
        "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Delectronics&field-keywords=laptop",
    ]
    rules = [Rule(LinkExtractor(allow=(['HP', 'Lenovo', 'Dell', 'Apple', 'Toshiba', 'ASUS', 'Intel', 'Acer', 'ref=sr_pg'])), callback="parse_items", follow=True)]

    def parse_items(self, response):
	global nPages;

	page_title = response.xpath("//head/title/text()").extract()
	print page_title

        sites = Selector(response).xpath("/html/body/div/div/div/div/div/h1/span")
	
	substr_list = ['lenovo', 'HP', 'Dell', 'Apple', 'Toshiba', 'ASUS', 'Intel', 'Acer']

	if page_title:
		for laptop_brand in substr_list:    
		    if laptop_brand in page_title[0] and nPages<=1000:						
				item = LaptopItem()
				price = 0
				for site in sites:
					#pdb.set_trace();    
				    	item['brand'] = site.xpath('text()').extract()
				    	item['cost'] = site.xpath('//table//span[@id="priceblock_ourprice"]/text()').extract()
				    	item['customerRatings'] = site.xpath('//div[@id="averageCustomerReviews"]//span[@id="acrPopover"]/@title').extract()
				    	if item['cost']:
						price = item['cost'][0]
						price = float(price[1:])

				if price<1000:
					nPages +=1
					with open('scrapy_webpages'+str(nPages)+'.html', "w") as f:
			    			f.write(response.body)
					return item
		    elif nPages>1000:
			#sys.exit("SUTDOWN!");
			print "pages 1000 done"
	else:
		print "no page title"
