from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.spiders import CrawlSpider,Rule
from scrapy.linkextractors import LinkExtractor
from tutorial.items import LaptopItem
from scrapy.exceptions import CloseSpider
from scrapy.utils.markup import remove_tags
import pdb
import sys

nPages=0;

class DmozSpider(CrawlSpider):
    name = "laptopsCrawl"
    allowed_domains = ["craigslist.org"]
    start_urls = [
        "https://losangeles.craigslist.org/search/sya?sort=rel&query=laptop",
    ]
    rules = [Rule(LinkExtractor(allow=(['/sys/', '/syd/', 'sya?'])), callback="parse_items", follow=True)]

    def parse_items(self, response):
	global nPages;

	page_title = response.xpath("//head/title/text()").extract()
	print page_title

        sites = Selector(response).xpath("/html/body")

	substr_list = ['Lenovo', 'HP', 'Dell', 'Apple', 'Toshiba', 'ASUS', 'Intel', 'Acer', 'Sony', 'Panasonic']

	if page_title:
		for laptop_brand in substr_list:    
		    if laptop_brand in page_title[0] and nPages<=2000:						
				item = LaptopItem()
				price = 0

				#for site in sites:
					#pdb.set_trace();    
				if sites.xpath('//section[@id="postingbody"]'):
					item['cost'] = remove_tags(str(sites.xpath('//section[@id="postingbody"]').extract()[0]))
					descrip = item['cost'].replace('\n',' ');
				

					#pdb.set_trace();
					nPages +=1
					#print "npages: "+str(nPages)
					with open('scrapy_webpages.html', "a") as f:
			    			f.write('\n Description: '+descrip+'\n Link: '+response.url+'\n')
					with open('counter.html', "w") as f:
			    			f.write(str(nPages))
					return item
		    elif nPages>2000:
			raise CloseSpider('1000 pages exceeded')
	else:
		print "no page title"
